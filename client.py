import socket
import random
import time

sock = socket.socket()
sock.connect(('localhost', 8695))

while True:
    message = sock.recv(1024)
    if message:
        response = random.randint(1,4)
        if response == 1:
            sock.sendall(bytes('Success', encoding='utf-8'))
            print('Success')
        print(message)
    else:
        pass
sock.close()
