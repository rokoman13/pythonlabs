import socket
import time
from select import select

sock = socket.socket()
sock.bind(('localhost', 8695))
sock.listen(1)

connection, address = sock.accept()
connection.settimeout(60)
sock.setblocking(0)
print("Robot connected from {address}".format(address=address))

while True:

    command = input()
    connection.send(bytes(command, encoding='utf-8'))
    time.sleep(1)
    try:
        message = connection.recv(1024)
        print('Received: ', message.decode())
    except socket.timeout:
        print('Retrying...')
        time.sleep(1)
        connection.send(bytes(command, encoding='utf-8'))
    else:
        print('Success!')
       
connection.close()
